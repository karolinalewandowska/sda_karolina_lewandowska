#include "gtest/gtest.h"
#include "Matematyka.hpp"

TEST(MatematykaTest, SilniaTest)
{
	Matematyka mat1;
	EXPECT_EQ(6, mat1.silnia(3));
}
TEST(MatematykaTest, FibonacciTest)
{
	Matematyka mat1;
	EXPECT_EQ(3, mat1.Fibonacci(4));
}
TEST(MatematykaTest, NWDTest)
{
	Matematyka mat1;
	EXPECT_EQ(4, mat1.NWD(12,4));
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
