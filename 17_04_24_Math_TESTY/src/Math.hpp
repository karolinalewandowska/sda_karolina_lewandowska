/*
 * Math.hpp
 *
 *  Created on: 03.05.2017
 *      Author: Karolina
 */

#ifndef MATH_HPP_
#define MATH_HPP_

#include <iostream>
#include <cmath>
#include <iomanip>

class Math {
public:
	Math();
	int factorial(int x);
	int Fibonacci(int y);
	int NWD(int u, int z);

};

#endif /* MATH_HPP_ */
