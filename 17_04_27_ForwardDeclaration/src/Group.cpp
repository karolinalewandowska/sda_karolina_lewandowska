

#include "Group.hpp"
#include "Unit.hpp"

void Group::resize() {
	if (mSize == 0) {
		mSize++;
		mUnits = new Unit*[mSize];
	} else {
		mSize++;
		Unit** newUnitArray = new Unit*[mSize];
		for (unsigned int i = 0; i <= mSize - 1; i++) {
			newUnitArray[i] = mUnits[i];
		}
		delete[] mUnits;
		mUnits = newUnitArray;
	}
}
Group::Group() :
		mUnits(0), mSize(0) {

}
void Group::add(Unit* unit) {
	resize();
	mUnits[mSize - 1] = unit;
}

void Group::clear() {
	for (unsigned int i = 0; i < mSize; i++) {
		delete mUnits[i]; // usuwaj� si� wska�niki, a nie obiekty
	}
	delete[] mUnits; //usuwaj� si� obiekty
	mSize = 0;
	mUnits = 0;
}

void Group::replicateGroup()

{
	unsigned int currentSize = mSize;

	for (unsigned int i = 0; i < currentSize; i++) {
		mUnits[i]->replicate();
	}

}

void Group::printUnits() {
	for (unsigned int i = 0; i < mSize; i++) {
		mUnits[i]->printID();
	}
}
Group::~Group() {
	clear();
}
