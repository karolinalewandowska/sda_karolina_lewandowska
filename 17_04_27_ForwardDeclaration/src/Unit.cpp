
#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Unit.hpp"
#include "Group.hpp"

Unit::Unit(std::string ID) :
		mID(ID), groupPtr(0) {
}
void Unit::printID() {
	std::cout << mID << std::endl;
}
void Unit::replicate() {
	int idVariation = rand() % 10;
	char newID = '0' + idVariation; //do chara dodajemy inta a chara mozemy dodac do stringa;

	Unit* newborn = new Unit(mID + newID);
	newborn->addToGroup(groupPtr);
	groupPtr->add(newborn); //tworzy nowa jednostke o tym samy ID, z wariacja �e bedzie dopisywac do ID jakas losowa liczbe;
}

void Unit::addToGroup(Group* group) {
	groupPtr = group;
}

