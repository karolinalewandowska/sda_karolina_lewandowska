

#ifndef UNIT_HPP_
#define UNIT_HPP_

#include <string>

class Group; //forward declaration

class Unit {

	std::string mID;
	Group* groupPtr;
public:
	Unit(std::string ID);
	void printID();
	void replicate();
	void addToGroup(Group*);
};

#endif /* UNIT_HPP_ */
