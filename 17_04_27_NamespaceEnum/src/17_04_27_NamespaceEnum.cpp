#include <iostream>

#include "circle.hpp"
#include "enum.hpp"
#include "figure.hpp"
#include "square.hpp"

using namespace std;

int main() {

	Geometry::Circle circle(3, Colour::green);
	Geometry::Square square(5, Colour::black);
	circle.print();
	square.print();

	Geometry::Figure* wsk;
	wsk = &circle;
	wsk->print();
	wsk = &square;
	wsk->print();
	return 0;
}
