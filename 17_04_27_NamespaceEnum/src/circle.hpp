/*
 * kolo.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef CIRCLE_HPP_
#define CIRCLE_HPP_

#include "enum.hpp"
#include "figure.hpp"
namespace Geometry {
class Circle: public Figure {
	int mRadius;
	Colour::Colour mColour;
public:
	Circle(int radius, Colour::Colour colour) :
			mRadius(radius), mColour(colour) {
	}
	virtual ~Circle() {

	}
	void print() {
		std::cout << "Circle radius: " << mRadius << std::endl;
		std::cout << "Circle colour: " << Colour::convertToString(mColour)
				<< std::endl;
	}
};
}
#endif /* CIRCLE_HPP_ */
