/*
 * enum.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "enum.hpp"

std::string Colour::convertToString(Colour colour) {
	std::string x = "";
	switch (colour) {
	case black:
		x = "black";
		break;
	case red:
		x = "red";
		break;
	case white:
		x = "white";
		break;
	case blue:
		x = "blue";
		break;
	case green:
		x = "green";
		break;
	default:
		x = "0";
	}
	return x;
}

