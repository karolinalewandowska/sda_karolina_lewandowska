/*
 * enum.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef ENUM_HPP_
#define ENUM_HPP_

#include <iostream>
namespace Colour {
enum Colour {
	black, red, white, blue, green
};
std::string convertToString(Colour colour);
}

#endif /* ENUM_HPP_ */
