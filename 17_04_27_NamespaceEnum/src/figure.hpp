/*
 * figura.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef FIGURE_HPP_
#define FIGURE_HPP_
#include <iostream>
#include "enum.hpp"
namespace Geometry {
class Figure {
public:
	virtual void print()=0;
	virtual ~Figure() {

	}
};
}
#endif /* FIGURE_HPP_ */
