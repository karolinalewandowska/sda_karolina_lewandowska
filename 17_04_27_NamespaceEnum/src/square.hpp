/*
 * kwadrat.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "figure.hpp"
namespace Geometry {
class Square: public Figure {
	int mSideLength;
	Colour::Colour mColour;
public:
	Square(int sideLength, Colour::Colour colour) :
			mSideLength(sideLength), mColour(colour) {
	}
	virtual ~Square() {

	}
	void print() {
		std::cout << "Length of the side of the square: " << mSideLength
				<< std::endl;
		std::cout << "Square colour: " << Colour::convertToString(mColour)
				<< std::endl;
	}

};

}

#endif /* SQUARE_HPP_ */
