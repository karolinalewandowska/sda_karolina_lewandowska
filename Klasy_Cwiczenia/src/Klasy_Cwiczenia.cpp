#include <iostream>

class Test
{
public:
	Test()
	{
		std::cout<< "Konstruktor Test"<< std::endl;
	}
	~Test()
	{
		std::cout << "Destruktor ~Test"<< std::endl;
	}
};

class Zadania: public Test
{
public:
		Zadania()
		{
			std::cout<< "Konstruktor Zadania"<< std::endl;
		}
		~Zadania()
		{
			std::cout << "Destruktor ~Zadania"<< std::endl;
		}
};

class Licznik
{
	int licznik; // w nowszym c++ mo�emy napisac = 0, ale nie w tym

public:
	Licznik()
	{
		licznik = 1; // tylko przypisanie w konstuktorze da nam pewnosc, ze nie bedzie tam �mieci
		std::cout<< "Konstruktor domy�lny" << std::endl;
	}
	Licznik(const Licznik& zrodlo) // Licznik zrodlo -przekazuje przez wartosc i wywo�a�by si� konstruktor,
	{// dlatego musi byc referencja Licznik& zrodlo

		licznik = zrodlo.licznik +1;
		std::cout<< "Konstruktor kopiuj�cy" << std::endl;
	}
	~Licznik()
	{
		std::cout<<"Wy�wietl licznik:"<< licznik << std::endl;
	}

};
//Do klasy z poprzedniego punktu napisz konstruktor kopiuj�cy i operator przypisania.
//Do ustawiania p�l w konstruktorach u�yj listy inicjalizacyjnej.

class DanaFloat
{
	float* danaFloat;
public:
	DanaFloat()
	: danaFloat(new float) //lub w klamrze danaFloat = new float; lub tu dane(new float(1.0f))
	{
		*danaFloat=1.0f; //lub dane =new float(1.0f);
		std::cout << "Konstruktor domy�lny "<< *danaFloat<<std::endl;
	}
	DanaFloat(const DanaFloat& zrodlo2)
	:danaFloat(new float) //lub w klamrze danaFloat = new float; lub dane(new float(*zrodlo.dane+0.5f)
	{
		*danaFloat = *zrodlo2.danaFloat + 1.9f;
		std::cout << "Konstruktor kopiuj�cy " << *danaFloat<< std::endl;
	}
	DanaFloat& operator=(const DanaFloat& zrodlo2) //operator przypisania
	{
//		delete danaFloat; //jest  prosto wiec tego moze nie byc, ale tak byloby ekstra poprawnie
//		danaFloat =new float;
		*danaFloat = *zrodlo2.danaFloat + 0.1f;
		std::cout<< "Operator przypisania: "<< *danaFloat<< std::endl;
		return *this;
	}
	~DanaFloat()
	{
		delete danaFloat;
		std::cout<< "Destruktor" << std::endl;
	}
};
void funkcja (Licznik licznik2) //obiekt przekazany przez warto�c, najpierw we�mie kopie =2 1 kopia bo przekazuje przez wartosc
{ // jakbysmy mieli tu referencje to wtedy zamiast 2 i 3 dostajemy 2
	Licznik licznik3(licznik2); // i potem j� drugi raz kopiuje = 3; druga kopia
}
void funkcjaDoFloata(DanaFloat danaFloat)
{
	DanaFloat danaFloat3(danaFloat); // jak destrukotr jest wywo�ywany w funkcji to jak wywo�ujemy funkcje to tez destruktor
}
int main()

{
//	Test test1; // instancja klasy stworzona na stosie // nie ma delete wiec destuktor wywoluje si� na ko�cu main
//
//	Test* test2 = new Test(); // instancja klasy stworzona dynamicznie, nawias bo jest bezargumentowy konstruktor
//	delete test2; // bo robi� tu delete to dlatego destuktor si� uruchamia od razu
//
//	Zadania zadania1;
//	Zadania* zadania2 = new Zadania();
//	delete zadania2;

//	Licznik licznik1;
//	Licznik licznik2;
//	funkcja(licznik2);
//	Licznik licznik3=licznik2; // uzyty licznik kopiuj�ct wiec otrzymamy 2

	DanaFloat danaFloat;
	DanaFloat danaFloat1(danaFloat);
	funkcjaDoFloata(danaFloat);
	DanaFloat danaFloat4 = danaFloat;
	DanaFloat danaFloat5;
	danaFloat5 = danaFloat; //operator przypisania tu dzia�a

	return 0;
}
