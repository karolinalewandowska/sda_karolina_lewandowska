
#include <iostream>
#include <cmath>
using namespace std;

class Punkt
{
protected:
	int mX;
	int mY;

public:
	Punkt()
	{

	}
	Punkt(int x, int y)
	:mX(x)
	,mY(y)
	{

	}
	int getX() const
	{
		return mX;
	}

	void setX(int x)
	{
		mX = x;
	}

	int getY() const
	{
		return mY;
	}

	void setY(int y)
	{
		mY = y;
	}
	void wypiszXY()
	{
		cout<< "["<< mX <<", "<< mY <<"]"<<endl;

	}

	void przesun(int a , int b)
	{
		przesunX(a); //korzystamy z prostszych funkcji, �eby si� nie powtarzac
		przesunY(b); // i �eby ka�da funkcja mia�a single responsibility
	}
	void przesun(Punkt punktPrzesun)
	{
		przesun(punktPrzesun.getX(), punktPrzesun.getY());
		//mY = mY + punktPrzesun.mY;
		//mX+=punktPrzesun.getX(); ta opcja nam bru�dzi jak tamta metoda zostalaby zmieniona

	}
	void przesunX(int a)
	{
		mX+=a; // je�li boimy si� �e kto� wpisze jakie� g�upoty to porobic tu warunki
		//i zabezpieczyc si�
	}
	void przesunY(int b)
	{
		mY+=b;
	}
	int obliczOdleglosc(Punkt punktOdleglosc)
	{
		return sqrt((pow(mX-punktOdleglosc.mX,2)+pow(mY-punktOdleglosc.mY,2)));
	}
	float obliczOdleglosc(int w, int z)
	{
		return sqrt((pow(mX-w,2)+pow(mY-z,2)));
	}
};
class KolorowyPunkt : Punkt
{
public:
enum Kolor
	{
		czerwony,
		niebieski,
		czarny,
		zielony
	};
Kolor mKolor;



KolorowyPunkt(int x, int y, Kolor kolor)
:Punkt(x,y)
,mKolor(kolor)
{

}
void wypiszXY()
{
	cout<< "["<< mX <<", "<< mY <<"]"<<endl;

	switch (mKolor)
	{
	case czerwony:
		cout<<"czerwony\n";
		break;
	case niebieski:
		cout<<"niebieski\n";
		break;
	case czarny:
		cout<<"czarny\n";
		break;
	case zielony:
		cout<<"zielony\n";
		break;
	default:
		break;
	}
}
};

int main()
{
//	Punkt punkt1(1,2);
//	Punkt punkt2(2,3);
//	punkt1.wypiszXY();
//	punkt2.wypiszXY();
//	punkt1.przesunX(1);
//	punkt2.przesunY(1);
//	punkt1.wypiszXY();
//	punkt2.wypiszXY();
//	punkt1.przesun(1,1);
//	punkt2.przesun(1,1);
//	punkt1.wypiszXY();
//	punkt2.wypiszXY();
//	cout<<punkt1.obliczOdleglosc(2,3)<<endl;
//	cout<<punkt1.obliczOdleglosc(punkt2)<<endl;
//	cout<<punkt2.obliczOdleglosc(20,30)<<endl;
//	Punkt punkt3(1,1);
//	cout<<punkt3.obliczOdleglosc(3,3)<<endl;

	KolorowyPunkt kolorowyPunkt1(3,7,KolorowyPunkt::czerwony);
	kolorowyPunkt1.wypiszXY();

	return 0;
}
