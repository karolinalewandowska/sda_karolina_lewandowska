/*
 * Kubeczek.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef CUP_HPP_
#define CUP_HPP_

#include "Liquid.hpp"
#include "TypesOfLiquids.hpp"
#include <cstdlib>

class Cup
{
private:
	Liquid** mContent;
	size_t mVolume;

	void resize()
	{
		if (mVolume == 0)
		{
			mContent = new Liquid*[++mVolume]; //tworze nowa tablice
		}
		else
		{
			Liquid** newLiquidArray = new Liquid*[++mVolume]; //tymczasowa tablica na p�yny o rozmairze o jeden wiekszy
			for (unsigned int i = 0; i <= mVolume - 1; i++) //kopiowanie starej tablicy do nowej
			{
				newLiquidArray[i] = mContent[i];
			}
			delete[] mContent; //usuwam wska�niki starej tablicy
			mContent = newLiquidArray; //nadpisuje wskaxnik w klasie na tmczasowa tablice kt�ra teraz nie ejst tymczasowa
		}
	}
	void clear()
	{
		for (size_t i = 0; i < mVolume; i++) // dla kazdego plynu w kubku
		{
			delete mContent[i]; //usuwaja sie obiekty a nie wskazniki
		}
		delete[] mContent;
		mContent = 0;
		mVolume = 0;
	}

	int calculateLiquidAmount()
	{
		int liquidAmount = 0;
		if (mVolume == 0)
		{
			//do nothing
		}
		else
		{

			for (size_t i = 0; i < mVolume; i++)
			{
				liquidAmount += mContent[i]->getAmount();
			}

		}
		return liquidAmount;
	}
public:
	Cup() :
			mContent(0), mVolume(0)
	{

	}

	void add(Liquid* liquid)
	{
		resize();
		mContent[mVolume - 1] = liquid; // TODO niezgodne z RAII bo obiekt jest przekazywany spoza naszej
										//klasy i ktos nam moze podrzucic co� ze stosu zamiast sterty
	}

	void add(Milk &liquid)
	{
		resize();
		mContent[mVolume-1] = new Milk(liquid);
	}
	void add(Coffee &liquid)
		{
			resize();
			mContent[mVolume-1] = new Coffee(liquid);
		}


	//TODO jak sprawdzic czy nie duplikujemy plynow??
	void takeSip(int amount)
	{
		int liquidAmount = calculateLiquidAmount();
		if (liquidAmount > 0 && amount > 0)
		{
			if (liquidAmount<=amount)
			{
				spill(); // teoretycznie powinna byc osobna funkcja(ale w miare bli�niacza do spill)
			}
			else
			{
				for(size_t i=0; i< mVolume; i++)
				{
					float liquidRatio = (float)mContent[i]->getAmount()/(float)liquidAmount; //(float) rzutowanie zmiana z inta na floata
					mContent[i]->remove(liquidRatio*amount);

				}
			}
		}
		else
		{
			//do nothing
		}
	}
	void spill()
	{
		for (size_t i = 0; i < mVolume; i++) // dla kazdego plynu w kubku
		{
			mContent[i]->removeAll(); //usuwaja sie obiekty a nie wskazniki
		}
		clear();
	}

	//operator +=
};

#endif /* CUP_HPP_ */
