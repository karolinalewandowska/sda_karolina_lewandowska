/*
 * Liquid.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef LIQUID_HPP_
#define LIQUID_HPP_

class Liquid
{
protected:
	int mAmount;
public:
	Liquid (int amount)
:mAmount(amount)
	{

	}
	virtual ~Liquid()
	{
	}
	virtual void add(int amount)=0;
	virtual void remove(int amount)=0;
	virtual void removeAll()=0;

	int getAmount() const //getter nie musi byc wirtualny, bo zwraca co� co jest w klasie liquid
	{
		return mAmount;
	}
};
#endif /* LIQUID_HPP_ */
