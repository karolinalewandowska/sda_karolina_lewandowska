/*
 * TypesOfCoffee.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef TYPESOFCOFFEE_HPP_
#define TYPESOFCOFFEE_HPP_

#include "TypesOfLiquids.hpp"

class Decaff : public Coffee
{
public:
	Decaff(int amount)
:Coffee(amount, 0)
{
}
		void add(int amount)
				{
		         mAmount +=amount;
				}
		void remove(int amount)
				{
				mAmount = (mAmount > amount) ? mAmount-amount:0;
				}
		void removeAll()
				{
				mAmount=0;
				}

};

class Espresso : public Coffee
{
public:
	Espresso(int amount)
:Coffee(amount, 32)
{
}
		void add(int amount)
				{
		         mAmount +=amount;
				}
		void remove(int amount)
				{
				mAmount = (mAmount > amount) ? mAmount-amount:0;
				}
		void removeAll()
				{
				mAmount=0;
				}
};


#endif /* TYPESOFCOFFEE_HPP_ */
