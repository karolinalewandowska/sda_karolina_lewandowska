/*
 * TypesOfLiquids.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef TYPESOFLIQUIDS_HPP_
#define TYPESOFLIQUIDS_HPP_

#include "Liquid.hpp"

class Milk: public Liquid
{
protected:
	float mFat;

public:
	Milk(int amount, float fat)
:Liquid(amount)
,mFat(fat)
{
}
	~Milk()
	{

	}
	void add(int amount)
		{
         mAmount +=amount;
		}
	void remove(int amount)
		{
		mAmount = (mAmount > amount) ? mAmount-amount:0;
		}
	void removeAll()
		{
		mAmount=0;
		}
};

class Rum : public Liquid
{
protected:
	enum Colour
	{	light,
		dark
	};
	Colour mColour;
public:
	Rum(int amount, Colour colour)
:Liquid(amount)
	,mColour(colour)
{
}
~Rum()
	{

	}
void add(int amount)
		{
         mAmount +=amount;
		}
void remove(int amount)
		{
		mAmount = (mAmount > amount) ? mAmount-amount:0;
		}
void removeAll()
		{
		mAmount=0;
		}
};
class Coffee : public Liquid
{
protected:
	int mCaffeine;
public:
	Coffee(int amount, int caffeine)
:Liquid(amount)
,mCaffeine(caffeine)
{
}
	virtual ~Coffee()
	{

	}
	virtual void add(int amount)//musi byc wirtualny aby moc wolac odpowiednie meody w klasach bazowych -decaff, espresso
		{
         mAmount +=amount;
		}
	virtual void remove(int amount)
		{
		mAmount = (mAmount > amount) ? mAmount-amount:0;
		}
	virtual void removeAll()
		{
		mAmount=0;
		}
};


#endif /* TYPESOFLIQUIDS_HPP_ */
