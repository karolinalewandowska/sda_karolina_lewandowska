#include <iostream>

//Wy�wietla nieparzyste liczby naturalne (od 0) a� do g�rnej granicy, otrzymanej na wej�ciu.

void wyswietlNieparzyste (int limit, int* wynik, int& iloscWynikow)
{
	iloscWynikow = 0;

	if (limit <0)
	{
		std::cout << "B��dna warto�c." << std::endl;
	}
	else
	{
		for (int i=0; i < limit; i++)
		{
			if (i % 2 == 0)
			{
				continue;
			}
			else
			{
				wynik [i/2] = i;
				iloscWynikow = iloscWynikow + 1;
			}
		}
	}
}

int main()
{

	std::cout << "Podaj g�rn� granic� liczb: " << std::endl;
	int liczba;
	std::cin >> liczba;

	int* tablicaLiczb = new int [liczba / 2];
	int nieparzyste = 0;

	wyswietlNieparzyste(liczba, tablicaLiczb, nieparzyste);

	for (int i=0; i < nieparzyste; i++)
	{
		std::cout << tablicaLiczb[i] <<" ";
	}
//	wyswietlNieparzyste();
	delete [] tablicaLiczb;
	return 0;
}
